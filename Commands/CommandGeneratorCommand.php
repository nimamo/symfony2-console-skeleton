<?php

namespace OnMe\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use MrRio\ShellWrap as sh;
class CommandGeneratorCommand extends Command
{
		protected $commandTemplate = <<<template
<?php

namespace OnMe\\Console\\Command;

use Symfony\\Component\\Console\\Command\\Command;
use Symfony\\Component\\Console\\Input\\InputArgument;
use Symfony\\Component\\Console\\Input\\InputInterface;
use Symfony\\Component\\Console\\Input\\InputOption;
use Symfony\\Component\\Console\\Output\\OutputInterface;

class %sCommand extends Command
{
		protected function execute(InputInterface \$input, OutputInterface \$output)
		{
		}

		protected function configure()
		{
				\$this->setName('%s')
				//	->setDescription('')
				//	->addArgument('arg1', InputArgument::REQUIRED, 'Argument Description')
				//	->addArgument('arg2', InputArgument::OPTIONAL, 'Argument Description')
				//	->addArgument('arg3', InputArgument::IS_ARRAY, 'Argument Description')
					;
		}
}
template;

		protected function execute(InputInterface $input, OutputInterface $output)
		{
				$className = $input->getArgument('ClassName');
				$commandName = $input->getArgument('commandname');

				$output->writeln('Copying command skeleton to Commands/' . $className . 'Command.php');

				$skelton = sprintf($this->commandTemplate, $className, $commandName);
				if (\file_put_contents(__DIR__.'/'.$className.'Command.php', $skelton))
				{
						$output->writeln('Command generated...');
				}
				else
				{
						$output->writeln('ERROR: Could not write to file');
						return -1;
				}
				$output->writeln('Dump composer autoloader...');
				sh::composer('dump-autoload');
				$output->writeln('Done.');
		}
		protected function configure()
		{
				$this->setName('generate:command')
					->setDescription('Generate command skelton')
					->addArgument('ClassName', InputArgument::REQUIRED, 'Class name to use for new command')
					->addArgument('commandname', InputArgument::REQUIRED, 'Command name to use for new command');
		}
}

